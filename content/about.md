+++
title = "Sobre Kírov, o sea, del autor"
+++

## Tengo un blog

 Como ya aparece en la descripción, este es un blog personal, con bastante negligencia, pero con el interés suficiente para escribir de vez en cuando. Hay algunas ideas que quisiera compartir y con estas publicaciones aprovecho de ensayar su argumentación y escritura. Ya saben, Montaigne y esas cosas. 

 Sobre mí. Bueno, hola, soy Kírov. Tengo un blog, como ya conté. Me interesan muchas cosas y me esfuerzo en no parecer pedante. Mi principal motivación es derrumbar todo el orden social y soy instalador profesional de bombas en [Colectivo Disonancia](https://colectivodisonancia.net).

## Disurbia

 Tomo algunas fotografías y las publico por [acá](https://www.flickr.com/disurbia).

 ![](https://gitlab.com/desidia/desidia.gitlab.io/-/raw/main/img/metro.jpg)


## Contacto

 Dejo mi correo y mi llave GPG por si me quieren contactar de manera segura:

* [kirov[@]riseup.net](mailto:kirov@riseup.net) 

* [371E A5D9 B04D 047E 8D36  9508 8D6C 03C0 21AA 3DB3](http://hkps.pool.sks-keyservers.net/pks/lookup?op=get&search=0x8D6C03C021AA3DB3)

## Colectiviza

Todo el contenido es [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es) a menos que se indique lo contrario.