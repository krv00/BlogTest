+++
title =  "De virus y tecnócratas"
tags = ["pandemia", "tecnocracia", "disonancia"]
date = "2020-04-23"

+++

[![](https://colectivodisonancia.net/wp-content/uploads/2020/04/Portada_Autoritarismo-1.png)](https://colectivodisonancia.net/2020/04/el-autoritarismo-en-tiempos-de-la-pandemia/)

En el Colectivo Disonancia hicimos una publicación que reflexiona sobre la situación actual de la pandemia y los posible peligros de autoritarismo a largo plazo.

Comentamos la compleja crisis que atraviesa el capitalismo y cómo esa inestabilidad favorecerá, probablemente, a nuevas formas de dominación centradas en el poder tecnocrático.

La publicación se puede ver acá: 
https://colectivodisonancia.net/2020/04/el-autoritarismo-en-tiempos-de-la-pandemia/